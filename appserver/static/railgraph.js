//# sourceURL=splunk_main.js


// Constants
// --- Dependancies for Splunk Web
const deps = [
    "splunkjs/mvc",
    "splunkjs/mvc/utils",
    "splunkjs/mvc/tokenutils",
    "underscore",
    "jquery",
    "splunkjs/mvc/simplexml",
    "splunkjs/mvc/layoutview",
    "splunkjs/mvc/simplexml/dashboardview",
    "splunkjs/mvc/chartview",
    "splunkjs/mvc/searchmanager",
    "splunkjs/mvc/savedsearchmanager",
    "splunkjs/mvc/postprocessmanager",
    "splunkjs/mvc/simplexml/urltokenmodel",
    "splunkjs/mvc/searchbarview",
    "splunkjs/mvc/searchcontrolsview",
    "splunkjs/mvc/tokenforwarder",
    "splunkjs/mvc/simpleform/formutils",
    "splunkjs/mvc/simplexml/eventhandler",
    "splunkjs/mvc/simplexml/searcheventhandler",
    "splunkjs/mvc/timerangeview",
    "moment",
    "splunkjs/mvc/simplexml/ready!"
];
// --- Station numbers -> names
const stations = {
    112: "CHW",
    111: "NTR",
    110: "MQP",
    109: "MQU",
    108: "EPP",
    107: "CHE",
    106: "CSH",
    105: "SHW",
    104: "NRW",
    103: "BLV",
    102: "KVE",
    101: "RSH",
    100: "CUD"
};
// --- Colours
const mono = "[\"0x5077d5\"]";
const vibrant = "[\"0x1e93c6\",\"0xf2b827\",\"0xd6563c\",\"0x6a5c9e\",\"0x31a35f\",\"0xed8440\",\"0x3863a0\",\"0xa2cc3e\",\"0xcc5068\",\"0x73427f\",\"0x11a88b\",\"0xea9600\",\"0x0e776d\",\"0xffb380\",\"0xaa3977\",\"0x91af27\",\"0x4453aa\",\"0x99712b\",\"0x553577\",\"0x97bc71\",\"0xd35c2d\",\"0x314d5b\",\"0x99962b\",\"0x844539\",\"0x00b290\",\"0xe2c188\",\"0xa34a41\",\"0x44416d\",\"0xe29847\",\"0x8c8910\",\"0x0b416d\",\"0x774772\",\"0x3d9988\",\"0xbdbd5e\",\"0x5f7396\",\"0x844539\"]"
// Date format
const dateFormat = "DD/MM/YYYY HH:mm:ss ZZ";
// --- Exclusion Selection state
// --- --- 0 = no selection, 1 = first selection made
let exclState = 0;
// Holds the current selection values (e.g. the first selection)
let currExcl = {};
// Has the Exclusions section been shown yet?
let exclShown = 0;
// Number of Exclusions created
let exclusions = 0;
// Eclusion search string
let exclSearch = "";
// Logger
var _log;


require(deps, function (mvc,
    utils,
    TokenUtils,
    _,
    $,
    DashboardController,
    LayoutView,
    Dashboard,
    ChartElement,
    SearchManager,
    SavedSearchManager,
    PostProcessManager,
    UrlTokenModel,
    SearchbarView,
    SearchControlsView,
    TokenForwarder,
    FormUtils,
    EventHandler,
    SearchEventHandler,
    TimeRangeView,
    moment
) {
    let pageLoading = true;
    let _start = new Date().getTime();

    // Create logger with moment
    _log = (msg, sev = "INFO") => {
        console.log(`${moment().format("HH:mm:ss DD/MM/YYYY Z")} ${sev} ${msg}`);
    };

    // Splunk Token models
    let urlTokens = mvc.Components.getInstance('url');
    let defaultTokens = mvc.Components.getInstance('default', { create: true });
    let submittedTokens = mvc.Components.getInstance('submitted', { create: true });

    urlTokens.on('url:navigate', function () {
        defaultTokens.set(urlTokens.toJSON());
        if (!_.isEmpty(urlTokens.toJSON()) && !_.all(urlTokens.toJSON(), _.isUndefined)) {
            submitTokens();
        } else {
            submittedTokens.clear();
        }
    });

    // Initialize tokens
    defaultTokens.set(urlTokens.toJSON());

    const parseURL = () => {
        let arr = window.location.pathname.split('/');
        let url = arr.join('/');
        if (url.indexOf('?') !== -1)
            url = url.replace(arr[arr.length - 1], '');
        return url;
    };

    const submitTokens = () => {
        let toks = Object.assign(defaultTokens.toJSON(), submittedTokens.toJSON());
        let url = `${parseURL()}?`;
        for (let i in toks) {
            if (toks.hasOwnProperty(i)) {
                url += `${encodeURIComponent(i)}=${encodeURIComponent(toks[i])}&`
            }
        }
        url = url.slice(0, -1);
        if (pageLoading) {
            window.history.replaceState(toks, document.title, url);
        } else {
            window.history.pushState(toks, document.title, url);
        }
    };

    const tokens = {
        get: function (tokenName) {
            return defaultTokens.get(tokenName);
        },

        set: function (tokenName, tokenValue) {
            defaultTokens.set(tokenName, tokenValue);
            submittedTokens.set(tokenName, tokenValue);
            submitTokens();
        },
        on: function (eventName, callback) {
            defaultTokens.on(eventName, callback);
        }
    };

    // Register listeners
    const exclPanel = $("#exclusions");
    const exclBody = $("#exclusions .card-text");
    const exclModalSave = $("#exclID");
    const clrExcl = $('#clrExcl');
    const submitExcl = $('#submitExcl');
    const toggleColour = $('#toggleColour');
    clrExcl.click(() => {
        _log("Clearing Exclusions...");
        // Remove exclusion elements
        $('.btn-group.excl').remove();
        // Reset search token
        exclSearch = "";
        tokens.set('excl', exclSearch);
        // Hide panel
        toggleElm(exclPanel, false);
        exclShown = 0;
    });
    submitExcl.click((e) => {
        let exclArray = [];
        exclBody.find('.btn-group.excl').each((i, e) => {
            let excl = {
                trainid: $(e).find('.tid').text(),
                start: $(e).find('.start').data('epoch'),
                end: $(e).find('.end').data('epoch')
            }

            _log(JSON.stringify(excl));
            exclArray.push(excl);
        });

        _log(`SUBMIT_EXCLUSIONS: count="${exclArray.length}"`);

        // Build search string
        if (exclArray.length > 0) {
            exclSearch = "";
            for (let i = 0; i < exclArray.length; i++) {
                const x = exclArray[i];
                // Extract values and convert JS epoch to Unix
                exclSearch += `NOT (trainid=${x.trainid} _time>${x.start / 1000} _time<${x.end / 1000}) AND `;
            }
            // Remove last AND
            exclSearch = exclSearch.slice(0, -5);
        }
        _log(`EXCLUSION_SEARCH: search="${exclSearch.replace('"', '\"')}}"`);
        tokens.set('excl', exclSearch);
    });
    exclModalSave.click((e) => {
        // Get updated date string
        let trgID = $(e.currentTarget).data('exclid');
        let start = $('#exclModal #startDP').val();
        let end = $('#exclModal #endDP').val();
        // Get updated epoch
        let startEpoch = moment(start, dateFormat).valueOf();
        let endEpoch = moment(end, dateFormat).valueOf();
        $('#exclModal #startDP').data('epoch', startEpoch);
        $('#exclModal #endDP').data('epoch', endEpoch);

        // Commit changes to target exclusion
        $(`#${trgID} .start`).text(start)
        $(`#${trgID} .start`).data('epoch', startEpoch);
        $(`#${trgID} .end`).text(end);
        $(`#${trgID} .end`).data('epoch', endEpoch);
        $('#exclModal').modal('hide');
    });
    toggleColour.click((e) => {
        let btn = $(e.currentTarget);
        if (btn.hasClass("mono")) {
            railgraph.settings.set("charting.seriesColors", mono);
        } else {
            railgraph.settings.set("charting.seriesColors", vibrant);
        }
        renderStationNames();
        btn.toggleClass("mono");
    });


    // Helper Functions
    // --- Toggles visibility of an element
    const toggleElm = (elm) => {
        elm.animate({ height: 'toggle', opacity: 'toggle' }, 'slow');
    };
    // Action functions
    // --- Click listener for exclusion btn
    const onExclClick = (e) => {

        let src = null;
        if (e.originalEvent.hasOwnProperty('explicitOriginalTarget'))
            src = e.originalEvent.explicitOriginalTarget.className.replace('btn ', '');
        else
            src = e.originalEvent.srcElement.className.replace('btn ', '');
        let id = e.currentTarget.id;
        // Get Data
        let data = {
            exclID: id,
            start: $(`#${id} .btn.start`).text(),
            end: $(`#${id} .btn.end`).text(),
            startEpoch: $(`#${id} .btn.start`).data('epoch'),
            endEpoch: $(`#${id} .btn.end`).data('epoch')
        };
        switch (src) {
            case "start":
            case "end":
            case "tid":
                updateModal(data);
                $('#exclModal').modal('show');
                break;
            case "icon-x":
            case "rm":
                _log(`EXCLUSION_CLICK: target="close"`);
                $(e.currentTarget).remove();
                break;

            default:
                _log(`EXCLUSION_CLICK: target="${src}"`);
                break;
        }
    }
    // --- Updates the Exclusion Modal with the target data
    const updateModal = (data) => {
        // Update body
        $('#exclModal #startDP').val(data.start);
        $('#exclModal #endDP').val(data.end);
        $('#exclModal #startDP').data('epoch', data.startEpoch);
        $('#exclModal #endDP').data('epoch', data.endEpoch);
        $('#exclModal #exclID').data("exclid", data.exclID);
    }
    // --- Creates an element to show an Exclusion
    const createExclusionElm = (props) => {
        // Get current list count to create id
        let id = exclusions;
        let excl = `<div class="btn-group excl" id="excl${id}">
            <a href="#" class="btn rm"><i class="icon-x"></i></a>
            <a href="#" class="btn tid" title="Train ID">${props.trainid}</a>
            <a href="#" class="btn start" data-epoch="${props.startEpoch}">${props.start}</a>
            <a href="#" class="btn end" data-epoch="${props.endEpoch}">${props.end}</a>
        </div>`;
        excl = $(excl);
        excl.click(onExclClick);
        exclBody.append(excl);
        exclusions++;
    };

    // Parse tokenized exclusions
    let exclToks = tokens.get('excl');
    if (exclToks) {
        let rex = /\([^\)]+\)/g;
        let matches = exclToks.match(rex);
        for (let i = 0; i < matches.length; i++) {
            let split = matches[i].split(' ');
            let excl = {
                trainid: split[0].replace('(trainid=', ''),
                startEpoch: split[1].replace('_time>', ''),
                endEpoch: split[2].replace('_time<', '').slice(0, -1),
            };
            excl.start = moment(excl.startEpoch * 1000).format(dateFormat);
            excl.end = moment(excl.endEpoch * 1000).format(dateFormat);
            _log(`PARSING_EXCL_TOKEN: idx="${i}" excl="${JSON.stringify(excl)}"`);
            if (exclShown === 0) {
                toggleElm(exclPanel);
                exclShown = 1;
            }
            createExclusionElm(excl);
        }
    } else {
        tokens.set('excl', "");
    }

    // Get Yesterday at 04:00:00 and now
    const now = moment();
    const yd4am = moment().subtract(1, 'd').hour(4).minute(0).seconds(0);
    // Parse possible time tokens
    const _tr = { earliest: defaultTokens.get("_tr.earliest"), latest: defaultTokens.get("_tr.latest") };
    _log(`TIME_TOKENS: ${JSON.stringify(_tr)}`);
    // If not time tokens present, set them
    if (!_tr.earliest) {
        _tr.earliest = yd4am.unix()
    }
    if (!_tr.latest) {
        _tr.latest = now.unix()
    }
    // Apply time range token
    tokens.set("_tr.earliest", _tr.earliest);
    tokens.set("_tr.latest", _tr.latest);
    _log(`TIME_TOKENS_PARSED: ${JSON.stringify(_tr)}`);

    //
    // SEARCH MANAGERS
    //
    var railgraphSrch = mvc.Components.getInstance('railgraphSrch');
    if (!railgraphSrch)
        railgraphSrch = new SearchManager({
            id: "railgraphSrch",
            search: mvc.tokenSafe("`mts_index` $excl$ | bin _time span=1m | eval station=substr(platform,0, 3)| stats latest(station) AS station BY trainid, _time| lookup traindetails.csv station OUTPUTNEW station_number station_name| eval station_number=((100 - 'station_number') + 112)| timechart usenull=f useother=f span=1m latest(station_number) AS station_number BY trainid | append [| search `mts_index` SEVERITY!=LEVEL_EV | timechart span=1m count | rename count AS issues]"),
            cancelOnUnload: true,
            earliest_time: mvc.tokenSafe("$_tr.earliest$"),
            latest_time: mvc.tokenSafe("$_tr.latest$"),
            status_buckets: 0,
            app: utils.getCurrentApp(),
            auto_cancel: 90,
            preview: false,
            runWhenTimeIsUndefined: false
        }, { tokens: true, tokenNamespace: "submitted" });

    // Results object
    const railResults = railgraphSrch.data("results");
    // Process finilized results
    const applyYAxis = (yAxis) => {
        yAxis.each((i, el) => {
            let y = $(el);
            y.text(stations[y.text()]);
        });
    };
    const renderStationNames = () => {
        let c = 0;
        let tick = setInterval(() => {
            let yAxis = $('#railgraph .highcharts-yaxis-labels text');
            if (yAxis && yAxis.hasOwnProperty('length') && yAxis.length > 1) {
                for (let i = 0; i < yAxis.length; i++) {
                    if ($(yAxis[i]).text() === "100") {
                        applyYAxis(yAxis);
                        clearInterval(tick);
                    }
                }
            } else {
                _log(`WAIT_YAXIS: ${c++}`);
            }
        }, 200);
    };
    railgraphSrch.on('search:done', function () {
        _log("SEARCH_DONE");
        renderStationNames();
    });

    // 
    // TIME RANGE INPUT
    // 
    const parseTimeString = (time) => {
        let t = time * 1000;
        if (Number.isNaN(t)) {
            t = moment(time * 1000);
            t = t.isValid() ? t.format(dateFormat) : time;
        } else {
            t = moment(t).format(dateFormat);
        }
        return t;
    };
    const timerange = new TimeRangeView({
        id: "timerange",
        managerid: "railgraphSrch",
        dialogOptions: { showPresetsRealTime: false },
        el: $("#timerange")
    }).render();
    // Update the railgraph search when the time range changes
    timerange.on("change", function () {
        let tr = timerange.val();
        _log(`TIME_TOKENS_UPDATED: ${JSON.stringify(tr)}`);
        tokens.set("_tr.earliest", tr.earliest_time);
        tokens.set("_tr.latest", tr.latest_time);
        // Update time-display
        $('.time-display > .from').text(parseTimeString(tr.earliest_time));
        $('.time-display > .to').text(parseTimeString(tr.latest_time));
    });


    // 
    // RAIL GRAPH CHART
    // 
    const railgraph = new ChartElement({
        id: "railgraph",
        managerid: "railgraphSrch",
        type: "line",
        el: $("#railgraph"),
        height: "800px",
        "charting.legend.placement": "bottom",
        "charting.drilldown": "all",
        "charting.axisTitleX.visibility": "collapsed",
        "charting.axisTitleY.visibility": "collapsed",
        "charting.axisLabelsY.minorTickVisibility": "show",
        "charting.axisLabelsY.minorTickSize": 1,
        "charting.chart.nullValueMode": "connect",
        "charting.axisY.maximumNumber": 112,
        "charting.axisY.minimumNumber": 99.5,
        "charting.axisY.reversed": false,
        "charting.chart.overlayFields": "issues",
        "charting.axisY2.enabled": 1,
        "charting.axisY2.minimumNumber": 0,
        "link.openSearch.text": "Go to the Search app",
        "link.exportResults.visible": false,
        "link.openSearch.visible": true,
        "link.visible": true,
        "charting.fieldColors": "{ issues: 0xd05757 }",
        "charting.seriesColors": mono
    }, { tokens: true }).render();

    // Respond to legend clicks
    railgraph.on("click:legend", function (e) {
        e.preventDefault(); // Prevent redirecting to the Search app
        // To do: respond to events
        _log(`CLICKED_LEGEND: ${JSON.stringify(e.name2)}`);
    });

    // Respond to chart clicks
    railgraph.on("click:chart", function (e) {
        // Parse click point
        let station_number = e.value2;
        let station_name = stations[station_number];
        let trainid = e.name2;
        let _time = e.value;
        let jsTime = moment(_time * 1000).format(dateFormat);
        _log(`EXCLUSION_POINT: ${jsTime} trainid="${trainid}" station="${station_name}" stationid="${station_number}"`);

        // Show Exclusions
        if (exclState === 1) {
            // Process double click drilldown
            if (currExcl.trainid === trainid && currExcl.startEpoch === _time * 1000) {
                _log('DOUBLE_CLICK_DRILLDOWN');
            } else {
                e.preventDefault(); // Prevent redirecting to the Search app
                if (exclShown === 0) {
                    toggleElm(exclPanel);
                    exclShown = 1;
                }

                createExclusionElm({
                    trainid: currExcl.trainid,
                    start: currExcl.start,
                    startEpoch: currExcl.startEpoch,
                    end: jsTime,
                    endEpoch: _time * 1000
                });

                // Reset exclState and currExcl
                exclState = 0;
                currExcl = {};
            }
        } else {
            e.preventDefault(); // Prevent redirecting to the Search app
            exclState = 1;
            currExcl = {
                trainid: trainid,
                start: jsTime,
                startEpoch: _time * 1000
            }
        }

        _log(`EXCLUSION_STATE: ${exclState} ${JSON.stringify(currExcl)}`);
    });

    _log(`DASHBOARD_READY: ${new Date().getTime() - _start}s`);

    DashboardController.ready();
    pageLoading = false;
});
#   Version 8.0.1
#
# This file is used to force a restart of the app on Splunk Cloud as
# debug/refresh is blocked.
#

[default]
enabled = <boolean>
* Set whether the restart setting is enabled
* Default: true
